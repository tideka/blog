---
title : 'Linux China Open Source Community announce closed'
date : 2024-02-23T14:50:00+08:00
draft : false
categories : "Linux News"
tags : 
    - "linux kernel"
    - "open source movement" 
    - "free softwrae movement"
    - "linux"
    - "community"
cover:
    image: "/posts/linux-cn-community-closure/linux-china-closure.png"
    relative: true
---

## “Linux中国”开源社区宣布关闭

2024年2月1日，“Linux中国”开源社区的创始人，“硬核老王”，在Linux.cn（Linux中国官方网站）和其社交媒体帐号上宣布了开源社区将永久关闭、无限期停运的消息。在公告中，“硬核老王”揭露了过去几年来，他一直在经济上遭遇慢性困难，并且指出开源精神在中国和世界都已经得到了巨大的进展。他总结到，由于最初的推进开源文化教育的目的已经很好地实现，这个开源社区的使命已经完成，继续运营的意义也已经降低；由于人工智能的不断进化，翻译校对工作不再需要那么多的志愿者，其核心的翻译组织LCTT的活跃成员也降低到只有一两位，不再具有社区的特征了。“硬核老王”已经五十岁了，他指出，对他而言长期维持这样一个开源社区，已经成为疲惫感的来源。

“硬核老王”同时也坦白，“Linux中国”一直处于亏损状态，当初年轻的理想主义越来越难以维持；他也没有能找到托付开源社区的人或机构。“总的来说，就是因为这些大大小小的原因，我做出了艰难的决定，结束这些年的坚持。对不起了，大家”在文中，他这样向读者感慨到。

Linux中国不是Linux基金会的附属机构或合作伙伴，但他们的坚持和理想主义，已经践行了Linux所传达的团结、开放的自由软件精神。正如创始人所说的那样，Linux背后的精神势必会继续传播。我们相信，这样一个社区的关闭，只是恢宏乐章中的一个意料外的音符。

## “Linux China” Open Source Community announce closed

On 2/1/2024, Hardcore Mr. Wang (硬核老王), one of the founders of the “Linux China” Open Source Community announced the community will be permanently closed and remain unmaintained in the future. He revealed that he was suffering financial in the past years, and pointed out that the open source spirit itself has been widely spread inside China and around world. He believe, as the original goal of advocating open source education is now partially achieved, and the destiny of the community came to its end, it will constantly become more meaningless if the community was insisted to maintain. Also, due to the constant evolution of AI which made the verification and translation job will no longer be that dependent on humans’ work, the community’s core team, LCTT, is also no longer active, for only several volunteers still do their part is enough. Now, the founder who is in his fifties, pointed out the community is losing its sense of collaboration as a community, and even became another source of pressure to him both physically and mentally.

Mr. Wang also admitted to the financial conditions of the community is not optimistic. The passion that came from the youth’s idealism is becoming harder and harder to maintain, and the founder failed to find a trustworthy institute or personnel to pass the community to. “Generally speaking, for reasons, I made the hard decision. Sorry to everyone.” He sighed in the announcement to the readers. 

Linux China is not an affiliate to the Linux Foundation or its partner, but what they did to maintain such a community for years, do put the spirit of free software behind Linux into practice. Just like what the founder said, Linux’s spirit will be spread further. And we believe, such a closure of a community, is only an unexpected note in the symphony of Free Software movement.
