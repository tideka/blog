---
title: "Hello World"
categories: "Notifications"
tags:
    - system maintenance
    - blogging
---

The site is online, if you see this page!

This site is brought to you by Netlify, HUGO, PaperMod and many other generous providers, without whose help, as I student without incomes, the site will not be maintained.

My name is Tide Ka, you can also call me *tdk* for short or 霞汐霖 in Chinese.

Have a nice time here in my site! And if you encountered any problem, please report it to my mailbox: admin[at]ctide.dev
